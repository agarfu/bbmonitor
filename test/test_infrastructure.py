#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
import sys
from mock import Mock
import platform
# Let's mock picamera if we are not running in a raspberry pi
if platform.machine() != 'armv7l':
    sys.modules['picamera'] = Mock()
from src.main import hello



class HelloTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_hello(self):
        bot = Mock()
        update = Mock()
        update.message.from_user.first_name = 'Test'
        hello(bot, update)
        update.message.reply_text.assert_called_once_with(u'Hello Test')
