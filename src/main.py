#!/usr/bin/python
# -*- coding: utf-8 -*-

from telegram.ext import Updater, CommandHandler
from picamera import PiCamera
from time import sleep
from DHT22 import sensor
import pigpio
import logging
import datetime
import os
import sys
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)


def hello(bot, update):
    update.message.reply_text(u'Hello {}'.format(update.message.from_user.first_name))


def _get_temp_and_humidity():
    pi = pigpio.pi()
    s = sensor(pi, 4)
    s.trigger()
    sleep(0.2)
    temp = s.temperature()
    humidity = s.humidity()
    s.cancel()
    pi.stop()
    return (temp, humidity)


def environment(bot, update):
    temp, humidity = _get_temp_and_humidity()
    update.message.reply_text(u"""Temp: {}c
Hum: {}%""".format(temp, humidity))


def picture(bot, update):
    camera = PiCamera()
    camera.vflip = True
    sleep(5)
    camera.exposure_mode = 'night'
    camera.flash_mode = 'torch'
    camera.capture('image.jpg')
    camera.close()
    t, h = _get_temp_and_humidity()
    caption = u"{} - {}c/{}%".format(datetime.datetime.now(), t, h)
    update.message.reply_photo(caption=caption, photo=open('image.jpg', 'rb'))


def main(cmdline=sys.argv[1:]):
    updater = Updater(os.environ['TELEGRAM_BOT_KEY'])

    updater.dispatcher.add_handler(CommandHandler('hello', hello))
    updater.dispatcher.add_handler(CommandHandler('environment', environment))
    updater.dispatcher.add_handler(CommandHandler('picture', picture))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
