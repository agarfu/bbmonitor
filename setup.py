#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup

setup_reqs = ['pytest-cov', 'pytest', 'flake8', 'mock', 'requests_mock==1.2.0']

install_reqs = ['pigpio', 'picamera', 'python-telegram-bot', 'numpy', 'imutils', 'opencv-python']

config = {
    'description': 'Baby monitor for raspberry pi 3',
    'author': 'Rene Martin',
    'author_email': 'agarfu@gmail.com',
    'url': 'https://gitlab.com/agarfu/bbmonitor',
    'download_url': 'https://gitlab.com/agarfu/bbmonitor',
    'version': '0.0.1',
    'install_requires': install_reqs,
    'scripts': [],
    'name': 'bbmonitor',
    'setup_requires': ['nose'],
    'tests_require': setup_reqs,
    'package_dir': {'bbmonitor': 'src'},
    'packages': ['bbmonitor'],
    'entry_points': {
            'console_scripts': [
                'bbmonitor = bbmonitor.main:main',
            ],
        },
    'data_files': [
        ('/usr/local/doc/bbmonitor', ['LICENSE',
                                      'README.md',
                                      'config.dist']),
    ]
}

setup(**config)
